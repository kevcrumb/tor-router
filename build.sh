#!/bin/sh

set -ue

if pidof -q dockerd ; then

        if docker inspect --type=image tor-router > /dev/null ; then
		echo 'Docker image "tor-router" already exists.'
		echo -n 'Delete it? [y/N] ' && read delete
	        if [ "$delete" = "y" ]; then
			docker rmi tor-router || (
				echo 'You may have to stop and delete any container using this image:'
				echo 'docker container stop <NAME>'
				echo 'docker container rm <NAME>'
			)
		fi
	fi

	cd src && docker build --tag tor-router . && cd -

        echo -n 'Enter the tor package version (with revision number) that got included: '
	read version

	archive=binaries/tor-router-${version}.img.tar
	mkdir -p binaries
	docker save --output=${archive} tor-router

	echo Image saved to ${archive}.

        echo
        echo Suggested git commit message:
	echo "  built and added ${archive#*/}"
else
	echo Failed. Please ensure that dockerd is running. >&2
fi
